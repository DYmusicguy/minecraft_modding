package net.dymusicguy.testmod.setup;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.RegistryObject;

public class ModItems {
    public static final RegistryObject<Item> FOSSIL = Registration.ITEMS.register("fossil", () ->
            new Item(new Item.Properties().tab(ItemGroup.TAB_MATERIALS)));
    static void register() {}
}